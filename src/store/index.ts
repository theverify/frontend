import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    name: ''
  },
  mutations: {
    changeToken(state, token) {
      state.token = token
    },
    updateName(state, name) {
      state.name = name
    }
  },
  actions: {},
  modules: {}
})

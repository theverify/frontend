import axios from 'axios'

export const api = axios.create({
  baseURL: 'https://api2.theverify.co/api/',
  timeout: 10000
})
